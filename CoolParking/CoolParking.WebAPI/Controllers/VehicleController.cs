﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/vehicles")]
    public class VehicleController : ControllerBase
    {
        private readonly IParkingService _parkingService;
        private Regex _regex = new Regex(Settings.VehicleIdPattern);

        public VehicleController()
        {
            _parkingService = InitService.GetParkingService();
        }

        [HttpGet]
        public ActionResult<List<Vehicle>> GetVehicles()
        {
            return Ok(_parkingService.GetVehicles().ToList());
        }

        [HttpGet("{id}")]
        public ActionResult<Vehicle> GetVehicle(string id)
        {
            if (!_regex.IsMatch(id))
                return BadRequest("Id повинно мати вигляд DV-2345-KJ");
            if (_parkingService.GetVehicles().FirstOrDefault(x => x.Id == id) == null)
                return NotFound("Транспорта з таким Id не існує");
            
            return Ok(_parkingService.GetVehicles().First(x => x.Id == id));
        }

        [HttpPost]
        public ActionResult<Vehicle> AddVehicle(VehicleDto vehicleDto)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Vehicle vehicle = new Vehicle(vehicleDto.Id, vehicleDto.VehicleType, vehicleDto.Balance);
                    _parkingService.AddVehicle(vehicle);                
                    return Created("CoolParking.BL/Models/Vehicle", vehicle);
                }
                else
                {
                    throw new Exception(ModelState.ToString());
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpDelete("{id}")]
        public ActionResult DellVehicle(string id)
        {
            if (!_regex.IsMatch(id))
                return BadRequest("Id повинно мати вигляд DV-2345-KJ");
            
            try
            {
                _parkingService.RemoveVehicle(id);
                return NoContent();
            }
            catch (ArgumentException)
            {
                return NotFound("Транспорта з таким Id не існує");
            }
            catch (InvalidOperationException)
            {
                return BadRequest("Не можливо забрати транспорт так як присутній борг");
            }
        }
    }
}