﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Services;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/transactions")]
    public class TransactionsController : ControllerBase
    {
        private readonly IParkingService _parkingService;
        private Regex _regex = new Regex(Settings.VehicleIdPattern);

        public TransactionsController()
        {
            _parkingService = InitService.GetParkingService();
        }

        [HttpGet]
        [Route("last")]
        public ActionResult<TransactionInfo[]> LastTransactions()
        {
            if (_parkingService.GetVehicles().Count > 0)
                return Ok(_parkingService.GetLastParkingTransactions());
            else
                return NoContent();
        }

        [HttpGet]
        [Route("all")]
        public ActionResult<string> AllTransactionsFromLog()
        {
            try
            {
                return Ok(_parkingService.ReadFromLog());
            }
            catch (InvalidOperationException e)
            {
                return NotFound(e.Message);
            }
        }
        
        [HttpPut]
        [Route("topUpVehicle")]
        public ActionResult<Vehicle> TopUpVehicle(TopUpVehicleDto dto)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (_parkingService.GetVehicles().FirstOrDefault(x => x.Id == dto.Id) == null)
                        return NotFound("Транспорта з таким Id не існує");
                    
                    _parkingService.TopUpVehicle(dto.Id, dto.Sum);

                    return Ok(_parkingService.GetVehicles().First(x => x.Id == dto.Id));
                }
                else
                {
                    throw new Exception(ModelState.ToString());
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}