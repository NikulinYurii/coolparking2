﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;

namespace CoolParking.WebAPI
{
    public static class InitService
    {
        private static ILogService logService = new LogService(Settings.LogPath);
        private static ITimerService withdrawTimer = new TimerService(Settings.WriteOffPeriod);
        private static ITimerService logTimer = new TimerService(Settings.WritingLogPeriod);
        private static IParkingService parkingService = new ParkingService(withdrawTimer, logTimer, logService);

        public static IParkingService GetParkingService()
        {
            return parkingService;
        }
    }
}