﻿using System;
using System.ComponentModel.DataAnnotations;
using CoolParking.BL.Models;

namespace CoolParking.WebAPI
{
    public class VehicleDto
    {
        [RegularExpression(@"[A-Z]{2}-[0-9]{4}-[A-Z]{2}", ErrorMessage = "Id повинно мати вигляд DV-2345-KJ")]
        public string Id { get; set; }
        [Range(0,3)]
        public VehicleType VehicleType { get; set; }
        [Range(0,Double.MaxValue)]
        public decimal Balance { get; set; }
    }
}