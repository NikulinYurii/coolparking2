﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CoolParking.WebAPI
{
    public class TopUpVehicleDto
    {
        [RegularExpression(@"[A-Z]{2}-[0-9]{4}-[A-Z]{2}", ErrorMessage = "Id повинно мати вигляд DV-2345-KJ")]
        public string Id { get; set; }
        [Range(0,Double.MaxValue)]
        public decimal Sum { get; set; }
    }
}