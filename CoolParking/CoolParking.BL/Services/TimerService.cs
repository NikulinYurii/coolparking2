﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using System.Timers;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        public event ElapsedEventHandler Elapsed;
        public double Interval { get; set; }
        private Timer _timer;
        
        public TimerService(double interval)
        {
            Interval = interval;
        }

        public void Start()
        {
            _timer = new Timer(Interval) {AutoReset = true};
            _timer.Elapsed += this.Elapsed;
            _timer.Start();
        }

        public void Stop()
        {
            _timer.Stop();
        }

        public void Dispose()
        {
            _timer.Dispose();
        }
    }
}
