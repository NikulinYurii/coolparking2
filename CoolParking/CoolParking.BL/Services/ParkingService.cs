﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private ITimerService _withdrawTimer;
        private ITimerService _logTimer;
        private ILogService _logService;
        private Parking _parking;
        private List<TransactionInfo> _transactionInfos = new List<TransactionInfo>();
 
        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _withdrawTimer = withdrawTimer;
            _withdrawTimer.Elapsed += WithdrawTimerOnElapsed;
            _withdrawTimer.Start();
            
            _logTimer = logTimer;
            _logTimer.Elapsed += LogTimerOnElapsed;
            _logTimer.Start();
            
            _logService = logService;
            _parking = Parking.GetInstance();
        }

        private void LogTimerOnElapsed(object sender, ElapsedEventArgs e)
        {
            lock (_transactionInfos)
            {
                var res = "";
                foreach (var el in _transactionInfos)
                {
                    res += el.ToString();
                }
                _logService.Write(res);
                _transactionInfos.Clear();
            }
        }

        private void WithdrawTimerOnElapsed(object sender, ElapsedEventArgs e)
        {
            foreach (var el in _parking.Vehicles)
            {
                var tariff = Settings.Tariff[el.VehicleType];
                if (el.Balance <= 0)
                {
                    var money = Settings.PenaltyRate * tariff;
                        
                    _parking.Balanse += money;
                    el.Balance -= money;
                    lock (_transactionInfos)
                    {
                        _transactionInfos.Add(new TransactionInfo(DateTime.Now, el.Id, money));
                    }
                }
                else if (el.Balance < tariff)
                {
                    var money = Math.Abs(el.Balance - tariff) * Settings.PenaltyRate + el.Balance;

                    _parking.Balanse += money;
                    el.Balance -= money;
                    lock (_transactionInfos)
                    {
                        _transactionInfos.Add(new TransactionInfo(DateTime.Now, el.Id, money));
                    }
                }
                else
                {
                    _parking.Balanse += tariff;
                    el.Balance -= tariff;
                    lock (_transactionInfos)
                    {
                        _transactionInfos.Add(new TransactionInfo(DateTime.Now, el.Id, tariff));
                    }
                }
            }
        }

        public decimal MoneyForCurrentPeriod()
        {
            decimal res = 0;
            lock (_transactionInfos)
            {
                foreach (var el in _transactionInfos)
                {
                    res += el.Sum;
                }
            }

            return res;
        }
        
        public decimal GetBalance()
        {
            return _parking.Balanse;
        }

        public int GetCapacity()
        {
            return _parking.PlacesCount;
        }

        public int GetFreePlaces()
        {
            return _parking.PlacesCount - _parking.Vehicles.Count;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return _parking.Vehicles.AsReadOnly();
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (_parking.PlacesCount == _parking.Vehicles.Count)
                throw new InvalidOperationException("Паркінг заповнений");
            if (_parking.Vehicles.Exists(x => x.Id == vehicle.Id))
                throw new ArgumentException("Транспорт з таким Id існує");
            
            _parking.Vehicles.Add(vehicle);
        }

        public void RemoveVehicle(string vehicleId)
        {
            if (!_parking.Vehicles.Exists(x => x.Id == vehicleId))
                throw new ArgumentException("Транспорта з таким Id не існує");
            if (_parking.Vehicles.First(x => x.Id == vehicleId).Balance < 0)
                throw new InvalidOperationException("Не можливо забрати транспорт так як присутній борг");
            
            var vehicle = _parking.Vehicles.First(x => x.Id == vehicleId);
            _parking.Vehicles.Remove(vehicle);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (!_parking.Vehicles.Exists(x => x.Id == vehicleId))
                throw new ArgumentException("Транспорта з таким Id не існує");
            if(sum < 0)
                throw new ArgumentException("Не можна поповнити рахунок на від'ємну суму");
            
            _parking.Vehicles.First(x => x.Id == vehicleId).Balance += sum;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            lock (_transactionInfos)
            {
                return _transactionInfos.ToArray();
            }
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }

        public void Dispose()
        {
            _withdrawTimer.Dispose();
            _logTimer.Dispose();
            _logService = null;
            _parking.Dispose();
            _transactionInfos = null;
        }
    }
}