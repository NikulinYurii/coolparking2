﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal InitialBalance { get; set; } = 0;
        public static int PlacesCount { get; set; } = 10;
        public static int WriteOffPeriod { get; set; } = 5000;
        public static int WritingLogPeriod { get; set; } = 60000;
        public static Dictionary<VehicleType, decimal> Tariff { get; set; } = new Dictionary<VehicleType, decimal>()
        {
            {VehicleType.PassengerCar, 2},
            {VehicleType.Truck,5}, 
            {VehicleType.Bus, (decimal) 3.5}, 
            {VehicleType.Motorcycle,1}
        };

        public static decimal PenaltyRate { get; set; } = (decimal) 2.5;
        public static string VehicleIdPattern = @"[A-Z]{2}-[0-9]{4}-[A-Z]{2}";

        public static string LogPath { get; set; } = @"..\..\Transactions.log";
    }
}