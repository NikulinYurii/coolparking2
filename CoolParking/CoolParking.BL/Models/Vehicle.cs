﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        private Regex _regex = new Regex(Settings.VehicleIdPattern);
        private string _id;
        public VehicleType VehicleType { get; }
        public string Id
        {
            get
            {
                return _id;
            }
            private set
            {
                if (_regex.IsMatch(value))
                    _id = value;
                else
                {
                    throw new ArgumentException("Id повинно мати вигляд DV-2345-KJ");
                }
            }
        }
        public decimal Balance { get; internal set; }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            if (balance < 0)
                throw new ArgumentException("Баланс повинен бути > 0");
            else
            {
                Balance = balance;
                Id = id;
                VehicleType = vehicleType;
            }
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            var s = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var n = "0123456789";
            var rand = new Random();

            var regNum = s[rand.Next(s.Length)].ToString() + s[rand.Next(s.Length)].ToString() + "-" + 
                         n[rand.Next(n.Length)] + n[rand.Next(n.Length)] + n[rand.Next(n.Length)] + n[rand.Next(n.Length)] + "-" +
                         s[rand.Next(s.Length)].ToString() + s[rand.Next(s.Length)].ToString();
            return regNum;
        }

        public override string ToString()
        {
            return $"{Id} {VehicleType} {Balance}";
        }
    }
}