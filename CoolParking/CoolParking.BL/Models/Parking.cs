﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking : IDisposable
    {
        public decimal Balanse { get; set; } 
        public List<Vehicle> Vehicles { get; }
        public int PlacesCount { get; }
        private static Parking _instance = null;
        
        private Parking()
        {
            Balanse = Settings.InitialBalance;
            PlacesCount = Settings.PlacesCount;
            Vehicles = new List<Vehicle>();
        }

        public static Parking GetInstance()
        {
            return _instance ??= new Parking();
        }

        public void Dispose()
        {
            _instance = null;
        }
    }
}