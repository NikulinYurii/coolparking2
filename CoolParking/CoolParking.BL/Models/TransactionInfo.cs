﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public DateTime DateTime { get; }
        public string VehicleId { get; }
        public decimal Sum { get; }
        
        public TransactionInfo(DateTime dateTime, string vehicleId, decimal money)
        {
            DateTime = dateTime;
            VehicleId = vehicleId;
            Sum = money;
        }

        public override string ToString()
        {
            return $"Дата: {DateTime} Ід транспорту: {VehicleId} Знято: {Sum}";
        }
    }
}