﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using CoolParking.UI_WebApi.Models;
using Newtonsoft.Json;

namespace CoolParking.UI_WebApi.Services
{
    public class ParkingServiceWebApi
    {
        private HttpClient client = new HttpClient();
        
        public void Dispose()
        {
            client.Dispose();
        }

        public string GetBalance()
        {
            var response = client.GetAsync("https://localhost:5001/api/parking/balance");
            var res = response.Result.Content.ReadAsStringAsync().Result;
            
            if (response.Result.StatusCode == HttpStatusCode.OK)
                return res;
            else
                throw new HttpRequestException(response.Result.StatusCode.ToString());
        }

        public int GetCapacity()
        {
            var response = client.GetAsync("https://localhost:5001/api/parking/capacity");
            var res = response.Result.Content.ReadAsStringAsync().Result;
            
            if (response.Result.StatusCode == HttpStatusCode.OK)
                return Convert.ToInt32(res);
            else
                throw new HttpRequestException(response.Result.StatusCode.ToString());
        }

        public int GetFreePlaces()
        {
            var response = client.GetAsync("https://localhost:5001/api/parking/freePlaces");
            var res = response.Result.Content.ReadAsStringAsync().Result;
            
            if (response.Result.StatusCode == HttpStatusCode.OK)
                return Convert.ToInt32(res);
            else
                throw new HttpRequestException(response.Result.StatusCode.ToString());
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            var response = client.GetAsync("https://localhost:5001/api/vehicles");
            var res = response.Result.Content.ReadAsStringAsync().Result;
            
            if (response.Result.StatusCode == HttpStatusCode.OK)
            {
                Vehicle[] vehicles = JsonConvert.DeserializeObject<Vehicle[]>(res);
                return vehicles.ToList().AsReadOnly();
            }
            else
                throw new HttpRequestException(response.Result.StatusCode.ToString());
        }

        public Vehicle AddVehicle(Vehicle vehicle)
        {
            var vehicleJson = JsonConvert.SerializeObject(vehicle);
            HttpContent content = new StringContent(vehicleJson, Encoding.UTF8, "application/json");
            var response = client.PostAsync("https://localhost:5001/api/vehicles", content);
            
            if (response.Result.StatusCode == HttpStatusCode.BadRequest)
                throw new HttpRequestException(response.Result.StatusCode.ToString());

            if (response.Result.StatusCode == HttpStatusCode.Created)
            {
                var res = response.Result.Content.ReadAsStringAsync().Result;
                Vehicle responseVehicle = JsonConvert.DeserializeObject<Vehicle>(res);
                return responseVehicle;
            }
            else
                throw new HttpRequestException(response.Result.StatusCode.ToString());
        }

        public void RemoveVehicle(string vehicleId)
        {
            var response = client.DeleteAsync("https://localhost:5001/api/vehicles/"+vehicleId);
            
            if (response.Result.StatusCode == HttpStatusCode.BadRequest)
                throw new HttpRequestException(response.Result.StatusCode.ToString());
            
            if (response.Result.StatusCode == HttpStatusCode.NotFound)
                throw new HttpRequestException(response.Result.StatusCode.ToString());
            
        }

        public Vehicle TopUpVehicle(string vehicleId, decimal sum)
        {
            var contentJson = JsonConvert.SerializeObject(new{id = vehicleId, sum});
            HttpContent content = new StringContent(contentJson, Encoding.UTF8, "application/json");
            var response = client.PutAsync("https://localhost:5001/api/transactions/topUpVehicle", content);
            
            if (response.Result.StatusCode == HttpStatusCode.BadRequest)
                throw new HttpRequestException(response.Result.StatusCode.ToString());
            
            if (response.Result.StatusCode == HttpStatusCode.NotFound)
                throw new HttpRequestException(response.Result.StatusCode.ToString());

            if (response.Result.StatusCode == HttpStatusCode.OK)
            {
                var res = response.Result.Content.ReadAsStringAsync().Result;
                Vehicle responseVehicle = JsonConvert.DeserializeObject<Vehicle>(res);
                return responseVehicle;
            }
            else
                throw new HttpRequestException(response.Result.StatusCode.ToString());
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            var response = client.GetAsync("https://localhost:5001/api/transactions/last");
            var res = response.Result.Content.ReadAsStringAsync().Result;
            
            if (response.Result.StatusCode == HttpStatusCode.OK)
                return JsonConvert.DeserializeObject<TransactionInfo[]>(res);
            else
                throw new HttpRequestException(response.Result.StatusCode.ToString());
        }

        public string ReadFromLog()
        {
            var response = client.GetAsync("https://localhost:5001/api/transactions/all");
            var res = response.Result.Content.ReadAsStringAsync().Result;
            
            if (response.Result.StatusCode == HttpStatusCode.NotFound)
                throw new HttpRequestException(response.Result.StatusCode.ToString());
            
            if (response.Result.StatusCode == HttpStatusCode.OK)
                return res;
            else
                throw new HttpRequestException(response.Result.StatusCode.ToString());
        }
    }
}