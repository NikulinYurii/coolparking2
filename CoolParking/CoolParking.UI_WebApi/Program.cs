﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoolParking.UI_WebApi.Models;
using CoolParking.UI_WebApi.Services;

namespace CoolParking.UI_WebApi
{
    public class Program
    {
        private static readonly List<string> _menu = new List<string>()
        {
            "1 - Вивести на екран поточний баланс Паркінгу",
            "2 - Вивести на екран кількість вільних місць на паркуванні (вільно X з Y)",
            "3 - Вивести на екран усі Транзакції Паркінгу за поточний період (до запису у лог)",
            "4 - Вивести на екран історію Транзакцій (зчитавши дані з файлу Transactions.log)",
            "5 - Вивести на екран список Тр. засобів , що знаходяться на Паркінгу",
            "6 - Поставити Транспортний засіб на Паркінг",
            "7 - Забрати Транспортний засіб з Паркінгу",
            "8 - Поповнити баланс конкретного Тр. засобу",
            "q - Вихід"
        };
        
        public static void Main(string[] args)
        {
            ParkingServiceWebApi parkingService = new ParkingServiceWebApi();

            string choise = "";
            
            while (true)
            {
                PrintMenu();
                choise = Console.ReadLine();
                switch (choise)
                {
                    case "1":
                        try
                        {
                            PrintBlue("Поточний баланс = " + parkingService.GetBalance());
                        }
                        catch (Exception e)
                        {
                            PrintRed(e.Message);
                        }
                        break;
                    case "2":
                        try
                        {
                            PrintBlue( $"Вільних місць {parkingService.GetFreePlaces()} з {parkingService.GetCapacity()}");
                        }
                        catch (Exception e)
                        {
                            PrintRed(e.Message);
                        }
                        break;
                    case "3":
                        try
                        {
                            foreach (var el in parkingService.GetLastParkingTransactions())
                            {
                                PrintBlue(el.ToString());
                            }
                        }
                        catch (Exception e)
                        {
                            PrintRed(e.Message);
                        }
                        break;
                    case "4":
                        try
                        {
                            PrintBlue(parkingService.ReadFromLog());
                        }
                        catch (Exception e)
                        {
                            PrintRed(e.Message);
                        }
                        break;
                    case "5":
                        try
                        {
                            PrintBlue(parkingService.GetVehicles().Aggregate("", (current, el) => current + (el.ToString() + "\n")));
                        }
                        catch (Exception e)
                        {
                            PrintRed(e.Message);
                        }
                        break;
                    case "6":
                        try
                        {
                            parkingService.AddVehicle(NewVehicle());
                        }
                        catch (Exception e)
                        {
                            PrintRed(e.Message);
                        }
                        break;
                    case "7":
                        PrintBlue("Id транспорту:  ");
                        var id = Console.ReadLine();
                        try
                        {
                            parkingService.RemoveVehicle(id);
                        }
                        catch (Exception e)
                        {
                            PrintRed(e.Message);
                        }
                        break;
                    case "8": 
                        PrintBlue("Id транспорту:  ");
                        var idVehicle = Console.ReadLine();
                        var money = StringMoneyToDecimal();
                        try
                        {
                            parkingService.TopUpVehicle(idVehicle, money);
                        }
                        catch (Exception e)
                        {
                            PrintRed(e.Message);
                        }   
                        break;
                    case "q": 
                        return;
                    default:
                        PrintRed("Не відомий пункт меню");
                        break;
                }
            } 
        }

        private static decimal StringMoneyToDecimal()
        {
            PrintBlue("Сума яку необхідно додати: ");
            var moneyString = Console.ReadLine();
            decimal money = 0;
            try
            {
                money = Convert.ToDecimal(moneyString);
                if (money < 0)
                {
                    PrintRed("Сума повинна бути > 0");
                    StringMoneyToDecimal();
                }
            }
            catch (Exception)
            {
                PrintRed("Не правильно вказана сума");
                StringMoneyToDecimal();
            }

            return money;
        }
        private static void PrintMenu()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            foreach (var el in _menu)
            {
                Console.WriteLine(el);
            }

            Console.ForegroundColor = ConsoleColor.White;
        }
        private static void PrintRed(string text)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(text);
            Console.ForegroundColor = ConsoleColor.White;
        }
        private static void PrintBlue(string text)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine(text);
            Console.ForegroundColor = ConsoleColor.White;
        }
        private static void PrintCyan(string text)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine(text);
            Console.ForegroundColor = ConsoleColor.White;
        }
        private static Vehicle NewVehicle()
        {
            VehicleType type = VehicleType.Bus;
            string id = "";
            decimal balance = 0;
            PrintCyan($"Виберіть тип автомобіля: 1 - {VehicleType.Bus}, 2 - {VehicleType.PassengerCar}, 3 - {VehicleType.Truck}, 4 - {VehicleType.Motorcycle}");
            bool exit = false;
            do
            {
                switch (Console.ReadLine())
                {
                    case "1":
                        type = VehicleType.Bus;
                        exit = true;
                        break;
                    case "2":
                        type = VehicleType.PassengerCar;
                        exit = true;
                        break;
                    case "3":
                        type = VehicleType.Truck;
                        exit = true;
                        break;
                    case "4":
                        type = VehicleType.Motorcycle;
                        exit = true;
                        break;
                    default:
                        PrintRed("Не вірно вибраний тип транспорту");
                        exit = false;
                        break;
                }
            } while (!exit);

            exit = false;
            PrintCyan("1 - Згенерувати Id, 2 - Вказати самостійно");
            do
            {
                switch (Console.ReadLine())
                {
                    case "1":
                        id = Vehicle.GenerateRandomRegistrationPlateNumber();
                        PrintBlue(id);
                        exit = true;
                        break;
                    case "2":
                        id = Console.ReadLine();
                        exit = true;
                        break;
                    default:
                        PrintRed("Виберіть 1 або 2");
                        exit = false;
                        break;
                }
            } while (!exit);
            
            exit = false;
            PrintCyan("Баланс:");
            do
            {
                try
                {
                    balance = Convert.ToDecimal(Console.ReadLine());
                    exit = true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            } while (!exit);
            
            return new Vehicle(){Id = id, VehicleType = type, Balance = balance};
        }
    }
}