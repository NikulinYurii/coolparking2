﻿using System;

namespace CoolParking.UI_WebApi.Models
{
    public class TransactionInfo
    {
        public DateTime DateTime { get; set; }
        public string VehicleId { get; set; }
        public decimal Sum { get; set; }
        
        public override string ToString()
        {
            return $"Дата: {DateTime} Ід транспорту: {VehicleId} Знято: {Sum}\n";
        }
    }
}