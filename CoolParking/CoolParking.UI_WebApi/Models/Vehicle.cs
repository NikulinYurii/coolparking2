﻿using System;

namespace CoolParking.UI_WebApi.Models
{
    public class Vehicle
    {
        public string Id { get; set; }
        public VehicleType VehicleType { get; set; }
        public decimal Balance { get; set; }
        
        public static string GenerateRandomRegistrationPlateNumber()
        {
            var s = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var n = "0123456789";
            var rand = new Random();

            var regNum = s[rand.Next(s.Length)].ToString() + s[rand.Next(s.Length)].ToString() + "-" + 
                         n[rand.Next(n.Length)] + n[rand.Next(n.Length)] + n[rand.Next(n.Length)] + n[rand.Next(n.Length)] + "-" +
                         s[rand.Next(s.Length)].ToString() + s[rand.Next(s.Length)].ToString();
            return regNum;
        }
        
        public override string ToString()
        {
            return $"{Id} {VehicleType} {Balance}";
        }
    }
}