﻿namespace CoolParking.UI_WebApi.Models
{
    public enum VehicleType
    {
        PassengerCar, Truck, Bus, Motorcycle
    }
}